# Roles and Permissions in Admin-fe

Dokumentasi ini untuk penjelasan & penggunaan teknis Roles and Permissions pada **admin-fe**

there's 6 table and model related
  - users
  - users_roles
  - roles
  - roles_permissions
  - permissions
  - users_permissions

# How to Use
 ##### From **users** to **permissions**
  - ###### Give Permission User
  ```php
  $user->givePermissionTo(array permissions);
  ```
  - ###### Withdraw Permission User
    This similar action to delete or destroy
  ```php
  $user->withdrawPermissionTo(array permissions);
  ```
  - ###### Refresh Permission User
    This similar action to update
  ```php
  $user->refreshPermissions(array permissions);
  ```
  
##### From **users** to **roles**
  - ###### Give Role User
  ```php
  $user->giveRoleTo(array permissions);
  ```
  - ###### Withdraw Role User
    This similar action to delete or destroy
  ```php
  $user->withdrawRoleTo(array permissions);
  ```
  - ###### Refresh Role User
    This similar action to update
  ```php
  $user->refreshRoleTo(array permissions);
  ```
  
##### From **roles** to **permissions**
  - ###### connect Roles to Permission
  ```php
  $role->permissions()->attach(array $id_permission);
  ```
  - ###### disconnect Role to Permission
    This similar action to delete or destroy
  ```php
  $role->permissions()->detach(array $id_permission);
  ```
  
#### Using in Middleware
```php
// pass 1 argument for role
Route::group(['middleware' => 'role:admin',], function() {
    Route::get('/admin', function(){
        return 'example admin panel';
    });
    // or  passing 2 argument with some permissions
    // this create more customise route, not only base on role
    Route::group(['middleware' => 'role:admin,delete-user'], function(){
        Route::get('/admin/users', function() {
            return 'delete user in admin panel';
        });
    });
});
```


##### Another Functionality
  - ###### check if user have permissions
  ```php
$user->can('read-user');
// or using list of array
$user->can(['read-user','delete-user','update-user']);

// example blade directive
@can('delete-user')
    <h2>delete</h2>
@endcan
  ```
  - ###### check if user have roles
  ```php
$user->hasRole('superadmin');

// example in blade directive
@if(auth()->user()->hasRole('admin'))
  ```
  